﻿namespace HiMvcFormsAuth.ViewModels
{
    public class UserInfoViewModel
    {
        public string Name { get; set; }
        public string Username { get; set; }
    }
}