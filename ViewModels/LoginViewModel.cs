﻿namespace HiMvcFormsAuth.ViewModels
{
    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Message { get; set; }
    }
}