﻿using HiMvcFormsAuth.Models;
using HiMvcFormsAuth.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace HiMvcFormsAuth.Controllers
{
    public class HomeController : Controller
    {
        private readonly AppDbContext db = new AppDbContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel loginViewModel)
        {
            ViewBag.Message = "Login";
            loginViewModel.Message = "";
            if (
                !string.IsNullOrEmpty(loginViewModel.Username) &&
                !string.IsNullOrEmpty(loginViewModel.Password)
            )
            {
                var user = await db.Users.FirstOrDefaultAsync(x => x.Username == loginViewModel.Username);
                if (user == null)
                {
                    loginViewModel.Message = "帳號或密碼不正確";
                    return View(loginViewModel);
                }
                var isPasswordMatching = BCrypt.Net.BCrypt.Verify(loginViewModel.Password, user.PasswordHash);
                if (!isPasswordMatching)
                {
                    loginViewModel.Message = "帳號或密碼不正確";
                    return View(loginViewModel);
                }
                //Roles.AddUserToRole(user.Username, "Admin");
                FormsAuthentication.SetAuthCookie(user.Username, false);
                return RedirectToAction("Profile");
            }
            return View(loginViewModel);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        [Authorize]
        [ActionName("Profile")]
        public async Task<ActionResult> UserProfile()
        {
            ViewBag.Message = "Profile";
            var username = User.Identity.Name;
            var user = await db.Users
                .Where(x => x.Username == username)
                .Select(x => new UserInfoViewModel
                {
                    Name = x.Name,
                    Username = x.Username
                })
                .FirstOrDefaultAsync();
            return View(user);
        }

        [Authorize(Roles = "God")]
        public ActionResult God()
        {
            ViewBag.Message = "God";
            return View();
        }

        [Authorize(Roles = "God,Mortal")]
        public ActionResult Mortal()
        {
            ViewBag.Message = "Mortal";
            return View();
        }

        [Authorize(Roles = "God,Admin")]
        public ActionResult Admin()
        {
            ViewBag.Message = "Admin";
            return View();
        }
    }
}