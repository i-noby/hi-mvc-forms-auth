﻿using System.Data.Entity;

namespace HiMvcFormsAuth.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
            : base("Data Source=localhost\\SQLEXPRESS;Initial Catalog=HiMvcFormsAuth;Integrated Security=True") { }

        public DbSet<User> Users { get; set; }
    }
}